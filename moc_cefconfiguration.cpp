/****************************************************************************
** Meta object code from reading C++ file 'cefconfiguration.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../CefConfiguration/cefconfiguration.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cefconfiguration.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CefConfiguration_t {
    QByteArrayData data[24];
    char stringdata0[521];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CefConfiguration_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CefConfiguration_t qt_meta_stringdata_CefConfiguration = {
    {
QT_MOC_LITERAL(0, 0, 16), // "CefConfiguration"
QT_MOC_LITERAL(1, 17, 16), // "setSelectedInput"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 17), // "setSelectedOutput"
QT_MOC_LITERAL(4, 53, 15), // "setSelectedView"
QT_MOC_LITERAL(5, 69, 21), // "on_buttonBack_clicked"
QT_MOC_LITERAL(6, 91, 23), // "on_buttonBack_2_clicked"
QT_MOC_LITERAL(7, 115, 25), // "on_buttonViewName_clicked"
QT_MOC_LITERAL(8, 141, 25), // "on_buttonSaveView_clicked"
QT_MOC_LITERAL(9, 167, 30), // "on_setOutputNameButton_clicked"
QT_MOC_LITERAL(10, 198, 29), // "on_buttonSetInputName_clicked"
QT_MOC_LITERAL(11, 228, 18), // "updateInputButtons"
QT_MOC_LITERAL(12, 247, 14), // "prevButtonText"
QT_MOC_LITERAL(13, 262, 13), // "newButtonText"
QT_MOC_LITERAL(14, 276, 19), // "updateOutputButtons"
QT_MOC_LITERAL(15, 296, 17), // "updateViewButtons"
QT_MOC_LITERAL(16, 314, 34), // "on_multiViewTypeSelector_acti..."
QT_MOC_LITERAL(17, 349, 4), // "arg1"
QT_MOC_LITERAL(18, 354, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(19, 378, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(20, 402, 27), // "on_buttonDeleteView_clicked"
QT_MOC_LITERAL(21, 430, 28), // "on_exportToCSVButton_clicked"
QT_MOC_LITERAL(22, 459, 30), // "on_exportToCSVButton_3_clicked"
QT_MOC_LITERAL(23, 490, 30) // "on_exportToCSVButton_2_clicked"

    },
    "CefConfiguration\0setSelectedInput\0\0"
    "setSelectedOutput\0setSelectedView\0"
    "on_buttonBack_clicked\0on_buttonBack_2_clicked\0"
    "on_buttonViewName_clicked\0"
    "on_buttonSaveView_clicked\0"
    "on_setOutputNameButton_clicked\0"
    "on_buttonSetInputName_clicked\0"
    "updateInputButtons\0prevButtonText\0"
    "newButtonText\0updateOutputButtons\0"
    "updateViewButtons\0on_multiViewTypeSelector_activated\0"
    "arg1\0on_pushButton_2_clicked\0"
    "on_pushButton_3_clicked\0"
    "on_buttonDeleteView_clicked\0"
    "on_exportToCSVButton_clicked\0"
    "on_exportToCSVButton_3_clicked\0"
    "on_exportToCSVButton_2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CefConfiguration[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x08 /* Private */,
       3,    0,  110,    2, 0x08 /* Private */,
       4,    0,  111,    2, 0x08 /* Private */,
       5,    0,  112,    2, 0x08 /* Private */,
       6,    0,  113,    2, 0x08 /* Private */,
       7,    0,  114,    2, 0x08 /* Private */,
       8,    0,  115,    2, 0x08 /* Private */,
       9,    0,  116,    2, 0x08 /* Private */,
      10,    0,  117,    2, 0x08 /* Private */,
      11,    2,  118,    2, 0x08 /* Private */,
      14,    2,  123,    2, 0x08 /* Private */,
      15,    2,  128,    2, 0x08 /* Private */,
      16,    1,  133,    2, 0x08 /* Private */,
      18,    0,  136,    2, 0x08 /* Private */,
      19,    0,  137,    2, 0x08 /* Private */,
      20,    0,  138,    2, 0x08 /* Private */,
      21,    0,  139,    2, 0x08 /* Private */,
      22,    0,  140,    2, 0x08 /* Private */,
      23,    0,  141,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   13,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CefConfiguration::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CefConfiguration *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setSelectedInput(); break;
        case 1: _t->setSelectedOutput(); break;
        case 2: _t->setSelectedView(); break;
        case 3: _t->on_buttonBack_clicked(); break;
        case 4: _t->on_buttonBack_2_clicked(); break;
        case 5: _t->on_buttonViewName_clicked(); break;
        case 6: _t->on_buttonSaveView_clicked(); break;
        case 7: _t->on_setOutputNameButton_clicked(); break;
        case 8: _t->on_buttonSetInputName_clicked(); break;
        case 9: _t->updateInputButtons((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 10: _t->updateOutputButtons((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->updateViewButtons((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 12: _t->on_multiViewTypeSelector_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->on_pushButton_2_clicked(); break;
        case 14: _t->on_pushButton_3_clicked(); break;
        case 15: _t->on_buttonDeleteView_clicked(); break;
        case 16: _t->on_exportToCSVButton_clicked(); break;
        case 17: _t->on_exportToCSVButton_3_clicked(); break;
        case 18: _t->on_exportToCSVButton_2_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CefConfiguration::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_CefConfiguration.data,
    qt_meta_data_CefConfiguration,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CefConfiguration::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CefConfiguration::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CefConfiguration.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int CefConfiguration::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
